FROM maven:3.5.3-jdk-8
MAINTAINER Bartosz Wróblewski <wroblewskib92@gmail.com>

WORKDIR /opt/github-service
COPY . .
RUN mvn clean install
EXPOSE 8080

CMD java -jar ./target/github.jar
