package pl.allegro.github;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pl.allegro.github.repositories.RepositoriesApi;
import pl.allegro.github.repositories.dto.Repository;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWireMock(port = 8081)
public class GithubIntegrationTests {
    public static final String REDIRECT_URL = "/swagger-ui.html#!/repositories/getRepository";

    public static final Integer STARS_COUNT = 15377;
    MockMvc mockMvc;
    @Autowired
    private WebApplicationContext context;
    @Autowired
    private RepositoriesApi repositoriesApi;
    @Value("${github.repos.timeout}")
    private int timeout;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Test
    public void testRepositoriesApiService() {
        stubFor(get(urlEqualTo("/repos/test/test1"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("mocks/test-test1.json")));

        ResponseEntity<Repository> repositoryResponse = repositoriesApi.getRepository("test", "test1");
        Repository repository = repositoryResponse.getBody();

        assertEquals("Status code not OK", HttpStatus.OK, repositoryResponse.getStatusCode());
        assertNotNull("Repository is null", repository);
        assertEquals("Repository name not OK", "ry/deno", repository.getFullName());
        assertEquals("Repository description not OK", "A secure TypeScript runtime on V8", repository.getDescription());
        assertEquals("Repository clone url not OK", "https://github.com/ry/deno.git", repository.getCloneUrl());
        assertEquals("Repository created date not OK", "2018-05-15T01:34:26Z", repository.getCreatedAt());
        assertEquals("Repository stargazers count not OK", STARS_COUNT, repository.getStars());
    }

    @Test
    public void testMockedGithubResponse() throws Exception {
        stubFor(get(urlEqualTo("/repos/test/test1"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("mocks/test-test1.json")));

        mockMvc.perform(MockMvcRequestBuilders.get("/repositories/test/test1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.fullName", is("ry/deno")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description", is("A secure TypeScript runtime on V8")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.cloneUrl", is("https://github.com/ry/deno.git")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.createdAt", is("2018-05-15T01:34:26Z")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.stars", is(15377)));
    }

    @Test
    public void testNotFoundResponse() throws Exception {
        stubFor(get(urlEqualTo("/repos/test/test2"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.NOT_FOUND.value())));

        mockMvc.perform(MockMvcRequestBuilders.get("/repositories/test/test2"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void testInternalServerErrorResponse() throws Exception {
        stubFor(get(urlEqualTo("/repos/test/test3"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())));

        mockMvc.perform(MockMvcRequestBuilders.get("/repositories/test/test3"))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }

    @Test
    public void testTimeoutedResponse() throws Exception {
        stubFor(get(urlEqualTo("/repos/test/test4"))
                .willReturn(aResponse()
                        .withFixedDelay(timeout + 100)
                        .withStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())));

        mockMvc.perform(MockMvcRequestBuilders.get("/repositories/test/test4"))
                .andExpect(MockMvcResultMatchers.status().isServiceUnavailable());
    }

    @Test
    public void testRedirectFromHomeToSwaggerDocs() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl(REDIRECT_URL));

    }
}
