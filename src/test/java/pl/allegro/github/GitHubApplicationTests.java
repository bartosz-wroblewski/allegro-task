package pl.allegro.github;

import org.junit.Assert;
import org.junit.Test;
import pl.allegro.github.converter.RepositoryDTOToRepositoryConverter;
import pl.allegro.github.github.dto.RepositoryDTO;
import pl.allegro.github.repositories.dto.Repository;

public class GitHubApplicationTests {

    public static final String CLONE_URL = "https://github.com/ry/deno.git";
    public static final String CREATED_AT = "2018-05-15T01:34:26Z";
    public static final String DESCRIPTION = "A secure TypeScript runtime on V8";
    public static final String FULL_NAME = "ry/deno";
    public static final int STARS = 14976;

    private RepositoryDTOToRepositoryConverter repositoryDTOToRepositoryConverter = new RepositoryDTOToRepositoryConverter();

    private Repository repository = new Repository()
            .cloneUrl(CLONE_URL)
            .createdAt(CREATED_AT)
            .description(DESCRIPTION)
            .fullName(FULL_NAME)
            .stars(STARS);

    private RepositoryDTO repositoryDTO = RepositoryDTO.builder()
            .cloneUrl(CLONE_URL)
            .createdAt(CREATED_AT)
            .description(DESCRIPTION)
            .fullName(FULL_NAME)
            .stargazersCount(STARS)
            .build();

    @Test
    public void testConversion() {
        Repository repositoryMapped = repositoryDTOToRepositoryConverter.convert(repositoryDTO);

        Assert.assertEquals("Conversion failed", repositoryMapped, repository);
    }

}
