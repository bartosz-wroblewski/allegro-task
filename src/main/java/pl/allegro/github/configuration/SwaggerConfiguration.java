package pl.allegro.github.configuration;

import com.google.common.base.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/***
 * Class responsible for Swagger configuration.
 */
@Configuration
class SwaggerConfiguration {

    /**
     * Predicate, that allows Swagger access only controllers from package listed below.
     *
     * @return Predicate<RequestHandler>
     */
    private static Predicate<RequestHandler> paths() {
        return RequestHandlerSelectors.basePackage("pl.allegro.github.repositories");
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(paths())
                .build();
    }
}
