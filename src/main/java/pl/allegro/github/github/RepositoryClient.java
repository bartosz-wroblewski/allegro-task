package pl.allegro.github.github;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.allegro.github.github.dto.RepositoryDTO;

import java.util.Optional;

@FeignClient(name = "repositoryClient", url = "${github.repos.url}", configuration = GithubRepositoryFeignConfiguration.class)
interface RepositoryClient {

    /**
     * Returns repository data for given name and owner from github API.
     *
     * @param owner          - Repository owner
     * @param repositoryName - Repository name
     * @return Optional<RepositoryDTO>
     */
    @RequestMapping("/{owner}/{repositoryName}")
    Optional<RepositoryDTO> findByOwnerAndRepositoryName(@PathVariable("owner") String owner, @PathVariable("repositoryName") String repositoryName);
}
