package pl.allegro.github.github;

import com.google.common.cache.CacheBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

/**
 * Class responsible for cache configuration.
 */
@Configuration
class GithubCacheConfiguration extends CachingConfigurerSupport {

    public static final String REPOSITORIES = "repositories";
    public static final String CACHE_MANAGER = "cacheManager";
    @Value("${github.repos.cache.ttl}")
    private int cacheTtl;
    @Value("${github.repos.cache.size}")
    private int cacheSize;

    @Bean
    @Override
    public CacheManager cacheManager() {
        GuavaCacheManager cacheManager = new GuavaCacheManager();
        CacheBuilder<Object, Object> cacheBuilder = CacheBuilder.newBuilder()
                .maximumSize(cacheSize)
                .expireAfterWrite(cacheTtl, TimeUnit.MILLISECONDS);
        cacheManager.setCacheBuilder(cacheBuilder);
        cacheManager.setCacheNames(Collections.singletonList(REPOSITORIES));

        return cacheManager;
    }

}