package pl.allegro.github.github;

import pl.allegro.github.github.dto.RepositoryDTO;

import java.util.Optional;

interface RepositoryService {

    /**
     * Returns `Optional` object with repository data, specified by owner and repositoryName.
     *
     * @param owner          - Repository owner
     * @param repositoryName - Repository name
     * @return Optional<RepositoryDTO>
     */
    Optional<RepositoryDTO> getRepositoryByOwnerAndRepositoryName(String owner, String repositoryName);
}
