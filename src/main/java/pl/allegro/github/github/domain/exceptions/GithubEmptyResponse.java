package pl.allegro.github.github.domain.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Response thrown when response from Github API returns empty response.
 * <p>
 * methodKey - identifier for Feign method, that was used.
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Github API internal error")
@Getter
public class GithubEmptyResponse extends GithubException {
    public GithubEmptyResponse(String methodKey) {
        super(methodKey);
    }
}
