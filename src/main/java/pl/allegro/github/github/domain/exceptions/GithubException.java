package pl.allegro.github.github.domain.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * General response thrown when response from Github API throws unknown error.
 * <p>
 * methodKey - identifier for Feign method, that was used.
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Github API internal error")
@AllArgsConstructor
@Getter
public class GithubException extends Exception {
    private final String methodKey;
}
