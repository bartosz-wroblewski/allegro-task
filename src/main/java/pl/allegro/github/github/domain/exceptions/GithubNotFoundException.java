package pl.allegro.github.github.domain.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Response thrown when data from Github is not fetchable (ie. when response code = 404). This exception comes with 404 status for current request.
 * <p>
 * methodKey - identifier for Feign method, that was used.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Repository not found")
@Getter
public class GithubNotFoundException extends GithubException {
    public GithubNotFoundException(String methodKey) {
        super(methodKey);
    }
}
