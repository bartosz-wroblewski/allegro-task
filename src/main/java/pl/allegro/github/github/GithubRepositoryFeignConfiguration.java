package pl.allegro.github.github;

import feign.Logger;
import feign.Request;
import feign.codec.ErrorDecoder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.allegro.github.github.domain.exceptions.GithubEmptyResponse;
import pl.allegro.github.github.domain.exceptions.GithubException;
import pl.allegro.github.github.domain.exceptions.GithubInternalServerErrorException;
import pl.allegro.github.github.domain.exceptions.GithubNotFoundException;

import static java.util.Objects.isNull;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

/**
 * Class responsible for Feign configuration
 */
@Configuration
class GithubRepositoryFeignConfiguration {

    @Value("${github.repos.timeout}")
    private int timeout;

    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    @Bean
    public Request.Options options() {
        return new Request.Options(timeout, timeout);
    }

    @Bean
    public ErrorDecoder errorDecoder() {
        return (methodKey, response) -> {
            if (isNull(response)) {
                return new GithubEmptyResponse(methodKey);
            } else {
                if (response.status() == NOT_FOUND.value()) {
                    return new GithubNotFoundException(methodKey);
                } else if (response.status() == INTERNAL_SERVER_ERROR.value()) {
                    return new GithubInternalServerErrorException(methodKey);
                } else {
                    return new GithubException(methodKey);
                }
            }
        };
    }
}
