package pl.allegro.github.github;

import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.allegro.github.repositories.dto.Repository;

import static pl.allegro.github.github.GithubCacheConfiguration.CACHE_MANAGER;
import static pl.allegro.github.github.GithubCacheConfiguration.REPOSITORIES;

@Log
@RequiredArgsConstructor
@Service
public class GithubRepositoryFacade {
    private final RepositoryService repositoryService;
    private final ConversionService mvcConversionService;

    @Cacheable(value = REPOSITORIES, cacheManager = CACHE_MANAGER)
    public ResponseEntity<Repository> getRepositoryByOwnerAndRepositoryName(String owner, String repositoryName) {
        return repositoryService.getRepositoryByOwnerAndRepositoryName(owner, repositoryName)
                .map(repository -> new ResponseEntity<>(mvcConversionService.convert(repository, Repository.class), HttpStatus.OK))
                .orElse(ResponseEntity.notFound().build());
    }
}
