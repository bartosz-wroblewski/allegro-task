package pl.allegro.github.github;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.allegro.github.github.dto.RepositoryDTO;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
class RepositoryServiceImpl implements RepositoryService {
    private final RepositoryClient repositoryClient;

    @Override
    public Optional<RepositoryDTO> getRepositoryByOwnerAndRepositoryName(String owner, String repositoryName) {
        log.trace(".getRepositoryByOwnerAndRepositoryName() | Fetching for repository with owner: {} and name: {}", owner, repositoryName);

        return repositoryClient.findByOwnerAndRepositoryName(owner, repositoryName);
    }
}
