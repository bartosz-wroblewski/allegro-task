package pl.allegro.github.github.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class RepositoryDTO {
    @JsonProperty(value = "full_name")
    private final String fullName;
    private final String description;
    @JsonProperty(value = "clone_url")
    private final String cloneUrl;
    @JsonProperty(value = "stargazers_count")
    private final Integer stargazersCount;
    @JsonProperty(value = "created_at")
    private final String createdAt;
}
