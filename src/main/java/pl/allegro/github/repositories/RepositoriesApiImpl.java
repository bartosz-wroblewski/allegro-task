package pl.allegro.github.repositories;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.allegro.github.github.GithubRepositoryFacade;
import pl.allegro.github.repositories.dto.Repository;

import java.net.SocketTimeoutException;

@Slf4j
@RestController
@RequiredArgsConstructor
class RepositoriesApiImpl implements RepositoriesApi {

    private final GithubRepositoryFacade githubRepositoryFacade;

    @Override
    @GetMapping("/repositories/{owner}/{repository-name}")
    public ResponseEntity<Repository> getRepository(@PathVariable("owner") String owner, @PathVariable("repository-name") String repositoryName) {
        log.trace(".getRepository() | Got request for repository with owner: {} and name: {}", owner, repositoryName);

        return githubRepositoryFacade.getRepositoryByOwnerAndRepositoryName(owner, repositoryName);
    }

    @ExceptionHandler
    ResponseEntity<Repository> handleServiceUnavailable(SocketTimeoutException e) {
        log.error("Request timed out!", e);
        return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
    }
}
