package pl.allegro.github.repositories.domain;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import pl.allegro.github.converter.RepositoryDTOToRepositoryConverter;

@Configuration
class WebConfiguration implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new RepositoryDTOToRepositoryConverter());
    }
}
