package pl.allegro.github.repositories.dto;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Repository
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-26T17:48:30.197+02:00")

public class Repository   {
  @JsonProperty("fullName")
  private String fullName = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("cloneUrl")
  private String cloneUrl = null;

  @JsonProperty("stars")
  private Integer stars = null;

  @JsonProperty("createdAt")
  private String createdAt = null;

  public Repository fullName(String fullName) {
    this.fullName = fullName;
    return this;
  }

  /**
   * Get fullName
   * @return fullName
  **/
  @ApiModelProperty(value = "")


  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public Repository description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(value = "")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Repository cloneUrl(String cloneUrl) {
    this.cloneUrl = cloneUrl;
    return this;
  }

  /**
   * Get cloneUrl
   * @return cloneUrl
  **/
  @ApiModelProperty(value = "")


  public String getCloneUrl() {
    return cloneUrl;
  }

  public void setCloneUrl(String cloneUrl) {
    this.cloneUrl = cloneUrl;
  }

  public Repository stars(Integer stars) {
    this.stars = stars;
    return this;
  }

  /**
   * Get stars
   * @return stars
  **/
  @ApiModelProperty(value = "")


  public Integer getStars() {
    return stars;
  }

  public void setStars(Integer stars) {
    this.stars = stars;
  }

  public Repository createdAt(String createdAt) {
    this.createdAt = createdAt;
    return this;
  }

  /**
   * Get createdAt
   * @return createdAt
  **/
  @ApiModelProperty(value = "")


  public String getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Repository repository = (Repository) o;
    return Objects.equals(this.fullName, repository.fullName) &&
        Objects.equals(this.description, repository.description) &&
        Objects.equals(this.cloneUrl, repository.cloneUrl) &&
        Objects.equals(this.stars, repository.stars) &&
        Objects.equals(this.createdAt, repository.createdAt);
  }

  @Override
  public int hashCode() {
    return Objects.hash(fullName, description, cloneUrl, stars, createdAt);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Repository {\n");
    
    sb.append("    fullName: ").append(toIndentedString(fullName)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    cloneUrl: ").append(toIndentedString(cloneUrl)).append("\n");
    sb.append("    stars: ").append(toIndentedString(stars)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

