package pl.allegro.github.home;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Home Controller, redirects from / to Swagger documentation.
 */
@Controller
@RequestMapping("/")
class HomeController {

    public static final String REDIRECT_URL = "/swagger-ui.html#!/repositories/getRepository";

    @GetMapping
    public String redirectToSwagger() {
        return "redirect:" + REDIRECT_URL;
    }
}
