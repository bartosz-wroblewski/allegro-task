package pl.allegro.github.converter;

import org.springframework.core.convert.converter.Converter;
import pl.allegro.github.github.dto.RepositoryDTO;
import pl.allegro.github.repositories.dto.Repository;

public class RepositoryDTOToRepositoryConverter implements Converter<RepositoryDTO, Repository> {

    @Override
    public Repository convert(RepositoryDTO repositoryDTO) {
        return new Repository()
                .cloneUrl(repositoryDTO.getCloneUrl())
                .createdAt(repositoryDTO.getCreatedAt())
                .description(repositoryDTO.getDescription())
                .fullName(repositoryDTO.getFullName())
                .stars(repositoryDTO.getStargazersCount());
    }
}
