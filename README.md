# Allegro Recruitment Task
## Github Service

### Table of contents
- [Requirements](#requirements)
- [Endpoints](#endpoints)
- [Technology Stack](#technology-stack)
- [Configuration properties](#configuration)
- [Production deployment](#production-deployment)


### Requirements

- Create a simple REST service which will return details of given Github repository. Details should
  include full name, description, clone url, number of stargazers and date of creation in ISO format.
- Endpoint should accept GET requests at `/repositories/{owner}/{repository-name}`
- Should be able to serve 20 requests per second
- Should contain seth of end-to-end tests that can be run using build tool
- Should have good design and quality of code
- Should be almost ready to deploy on production 

### Endpoints
#### `/v1/repositories/{owner}/{repositoryName}`

Returns repository named `repositoryName` with owner named `owner` from github.
Returns `404` if repository not found or repository is private.

### Technology stack

- Java 8
- SpringBoot 2.0.2
- Swagger 2.2.7
- Spring Security 2.0.0 & OAuth
- Feign 1.4.4
- Lombok 1.18.0
- Wiremock 2.0.0

### Configuration

- `LOG_LEVEL` - Application log level, default: `INFO`
- `CLIENT_ID` - OAuth2 client id
- `CLIENT_SECRET` - OAuth2 client secret
- `ACCESS_TOKEN_URI` - OAuth2 access token URI, default: `https://github.com/login/oauth/access_token`
- `AUTH_URI` - OAuth2 authorization URI, default: `https://github.com/login/oauth/authorize?scope=user:email,read`
- `AUTH_TOKEN_NAME` - OAuth2 token name, default: `oauth_token`
- `SERVER_PORT` - Server port, default: `8080`
- `GITHUB_REPOS_URL`: - Github endpoint for repositories, default: `https://api.github.com/repos`
- `GITHUB_REPOS_TIMEOUT` - Github repository endpoint timeout in milliseconds, default: `500`
- `GITHUB_REPOS_CACHE_TTL` - Github repository endpoint cache ttl in milliseconds, default `5000`
- `GITHUB_REPOS_CACHE_SIZE` - Github repository endpoint cache size, default: `1024`

### Production deployment

Follow steps listed below to deploy this microservice to production.

- Configure your OAuth2 service
- Configure Jenkins:
    + Add new `Pipeline` job to Jenkins with definition `Pipeline script from SCM`
    + Select `git` SCM, add repository URL and credentials (recommended: `git@bitbucket.org:wroblewskib/github-api.git` as repository URL & ssh key)
    + Add new credentials for docker registry named `docker-hub-credentials`
- Configure `Jenkinsfile`
    + Change: registry URL (default `https://registry.hub.docker.com`) and repository name (default: `zb0oj/githubapi`)
    + Implement deployment section (also for test/uat enviroments if exists)
    + Add more magic to pipeline, ie. quality gateway from sonar - depending on organization standards
- Configure your favorite container orchestration software, all application properties can be set via environment variables.

Done!

### Requirements coverage

#### Should be able to serve 20 requests per second
Application was tested by jmeter script with sample data (10 repositories) and should handle more than 90 requests/s on 50 simultaneous threads (`stress-test.jmx` file included in sources).

![JMeter](https://i.ibb.co/GJQxdCD/jmeter.png)

#### Should contain seth of end-to-end tests that can be run using build tool
Project contains 7 unit tests that covers 60.1% of code (can be much higher without lombok attached).

![SonarQube](https://i.ibb.co/b5nRzhK/sonarqube.png)

#### Should be almost ready to deploy on production 
Project contain jenkins pipeline, which can be example for production deployment.

![Jenkins pipeline](https://image.ibb.co/ht9sJT/jenkins.png)